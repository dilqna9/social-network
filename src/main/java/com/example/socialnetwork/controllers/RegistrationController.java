package com.example.socialnetwork.controllers;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.User;
import com.example.socialnetwork.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Controller
public class RegistrationController {

    private final UsersService usersService;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UsersService usersService,
                                  UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user,
                               @RequestParam("imagefile") MultipartFile multipartFile,
                               BindingResult bindingResult,
                               Model model) {

        try {

            if (bindingResult.hasErrors()) {
                model.addAttribute("error", "Username and password must not be empty.");
                return "register";
            }

            if (userDetailsManager.userExists(user.getUsername())) {
                model.addAttribute("error", "User with the same username already exists.");
                return "register";
            }

            if (!user.getPassword().equals(user.getPasswordConfirmation())) {
                model.addAttribute("error", "Password doesn't match.");
                return "register";
            }

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(
                            user.getUsername(),
                            passwordEncoder.encode(user.getPassword()),
                            authorities);

            userDetailsManager.createUser(newUser);

            User registerUser = usersService.getUserByName(newUser.getUsername());

            registerUser.setFirstName(user.getFirstName());
            registerUser.setLastName(user.getLastName());
            registerUser.setEmail(user.getEmail());
            registerUser.setEnabled(true);
            registerUser.setProfilePicture(Base64.getEncoder().encode(multipartFile.getBytes()));

            usersService.updateUser(registerUser);


        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error",  e.getMessage());
            return "register";
        }
        return "redirect:/";
    }
}
