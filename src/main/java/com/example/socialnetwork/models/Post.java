package com.example.socialnetwork.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Post extends BaseEntity{

    private User creator;
    private String title;
    private String description;
    private byte[] picture;
    private Set<Category> categories;
    private Set<Comment> comments;

    public Post() {
    }

    @ManyToOne
    @JoinColumn(name = "creator_id")
    public User getCreator() {
        return creator;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    @Lob
    @Column(name = "picture")
    public byte[] getPicture() {
        return picture;
    }

    @ManyToMany
    @JoinTable(
            name = "posts_categories",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    public Set<Category> getCategories() {
        return categories;
    }

    @OneToMany(mappedBy = "post")
    public Set<Comment> getComments() {
        return comments;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
