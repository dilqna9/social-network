package com.example.socialnetwork.models;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User extends BaseEntity{

    private String username;
    private String password;
    private String passwordConfirmation;
    private String email;
    private String firstName;
    private String lastName;
    private byte[] profilePicture;
    private boolean enabled;

    public User() {
    }

    @Column(name = "username")
    @Size(min = 3, max = 50, message = "Username must be between 3 and 15 characters")
    public String getUsername() {
        return username;
    }

    @Column(name = "password")
    @Size(min = 6, max = 68, message = "Password must be between 6 and 68 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    public String getPassword() {
        return password;
    }

    @Transient
    @Size(min = 6, max = 68, message = "Password must be between 6 and 68 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    @Column(name = "email")
    @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$")
    public String getEmail() {
        return email;
    }

    @Column(name = "first_name")
    @Size(min = 3, max = 15, message = "First name must be between 3 and 15 characters")
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "last_name")
    @Size(min = 3, max = 15, message = "Last name must be between 3 and 15 characters")
    public String getLastName() {
        return lastName;
    }

    @Lob
    @Column(name = "profile_picture")
    public byte[] getProfilePicture() {
        return profilePicture;
    }

    @Column(name = "enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setProfilePicture(byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
