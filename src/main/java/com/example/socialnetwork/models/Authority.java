package com.example.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "authorities")
public class Authority extends BaseEntity {

    private String username;
    private String authority;

    public Authority() {
    }


    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    @Column(name = "authority")
    public String getAuthority() {
        return authority;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
