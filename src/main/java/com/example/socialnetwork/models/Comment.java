package com.example.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comment extends BaseEntity{

    private User creator;
    private String comment;
    private Post post;

    public Comment() {
    }

    @OneToOne
    @JoinColumn(name = "creator_id")
    public User getCreator() {
        return creator;
    }

    @Column(name = "description")
    public String getComment() {
        return comment;
    }

    @ManyToOne
    @JoinColumn(name = "post_id")
    public Post getPost() {
        return post;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
