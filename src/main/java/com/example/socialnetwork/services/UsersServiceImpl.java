package com.example.socialnetwork.services;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.User;
import com.example.socialnetwork.repositories.contracts.UsersRepository;
import com.example.socialnetwork.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.example.socialnetwork.utils.Constants.DOES_NOT_EXIST;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void updateUser(User user) {
        if(!usersRepository.checkUserExists(user.getUsername())){
            throw new EntityNotFoundException(String.format(DOES_NOT_EXIST, "user", "username", user.getUsername()));
        }
        usersRepository.updateUser(user);
    }

    @Override
    public User getUserByName(String name) {
        try {
            return usersRepository.getUserByName(name);
        }catch (EntityNotFoundException e){
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
