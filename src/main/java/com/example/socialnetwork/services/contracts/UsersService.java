package com.example.socialnetwork.services.contracts;

import com.example.socialnetwork.models.User;

public interface UsersService {

    void updateUser(User user);

    User getUserByName(String name);
}
