package com.example.socialnetwork.utils;

public final class Constants {

    public static final String DOES_NOT_EXIST = "%s with %s %s does not exist";
    public static final String ALREADY_EXISTS = "%s with %s %s already exists";
}
