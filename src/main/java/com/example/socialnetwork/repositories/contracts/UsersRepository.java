package com.example.socialnetwork.repositories.contracts;

import com.example.socialnetwork.models.User;

public interface UsersRepository {

    void updateUser(User user);

    User getUserByName(String name);

    boolean checkUserExists(String name);
}
