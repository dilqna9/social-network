package com.example.socialnetwork.repositories;

import com.example.socialnetwork.exceptions.EntityNotFoundException;
import com.example.socialnetwork.models.User;
import com.example.socialnetwork.repositories.contracts.UsersRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.example.socialnetwork.utils.Constants.DOES_NOT_EXIST;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void updateUser(User user) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getUserByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where username = :name", User.class);
            query.setParameter("name", name);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format(DOES_NOT_EXIST, "user", "name", name));
            }
            return query.list().get(0);
        }
    }

    @Override
    public boolean checkUserExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where username = :name", User.class);
            query.setParameter("name", name);

            return query.list().size() != 0;
        }
    }


}
